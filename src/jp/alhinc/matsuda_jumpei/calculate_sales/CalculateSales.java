﻿package jp.alhinc.matsuda_jumpei.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;


public class CalculateSales {
	public static void main(String[] args) {
		//支店コードと支店名のmap
		HashMap<String,String> nameMap = new HashMap<String,String>();
		//支店コードと売上金額のmap
		HashMap<String,Long> saleMap = new HashMap<String,Long>();
		//売上金額の合計を入れる箱
		long total = 0L;

		FileReader fr = null;
		BufferedReader br = null;

		try {
			//コマンドライン引数が1つかどうかの確認
			if(args.length != 1) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}
			File shopFile = new File(args[0],"branch.lst");
			//支店定義ファイルが存在するかどうかの確認
			if(shopFile.exists()) {
				fr = new FileReader(shopFile);
				br = new BufferedReader(fr);
				String line;
				while((line = br.readLine()) != null) {
					String[] nameDatas = line.split(",");
					//支店コードが3桁の数値であるかの確認
					if(nameDatas[0].matches("[0-9]{3}")) {
						//支店定義ファイルの1行あたりに、要素数が2つだけ存在するかの確認
						if(nameDatas.length == 2) {
							nameMap.put(nameDatas[0],nameDatas[1]);
							saleMap.put(nameDatas[0], 0L);
						}else {
							System.out.println("支店定義ファイルのフォーマットが不正です");
							return;
						}
					}else {
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}
				}
			}else {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}finally {
			if(br != null){
				try{
					br.close();
					fr.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}


		//売上ファイルを格納する箱の宣言
		ArrayList<File> SaleFileList = new ArrayList<File>();
		//連番確認用の、8桁の数字を格納する箱の宣言
		ArrayList<Integer> Numbers = new ArrayList<Integer>();
		File saleFile = new File(args[0]);
		//フォルダ内のファイルを読み込む
		File[] files= saleFile.listFiles();;

		try {
			//8桁の数字かつ.rcdのファイルの選別
			//8桁の数字をNumbersへ格納する
			for(int i=0;i<files.length; i++){
				//ファイル名を読み込む
				String fileName = files[i].getName();
				//8桁かつ.rcdを含むファイルを取り出す
				if(fileName.matches("^[0-9]{8}.rcd$") && files[i].isFile()) {
					//ファイル名の8桁の数字を取り出して、int型に変換、箱へ格納
					String nameOfNumber = fileName.substring(0,8);
					int number = Integer.parseInt(nameOfNumber);
					Numbers.add(number);
					//連番の確認
					for(int x=0; x<Numbers.size()-1; x++) {
						if(Numbers.get(x+1) - Numbers.get(x) == 1 ){
						}else {
							System.out.println("売上ファイル名が連番になっていません");
							return;
						}
					}
					SaleFileList.add(files[i]);
				}
			}

			//売上ファイルを読み込む工程
			for(int i=0; i<SaleFileList.size(); i++){
				//売上ファイルの、読込データを格納する箱
				ArrayList<String> Sales = new ArrayList<String>();
				//ArrayListから1つずつ取り出す
				fr = new FileReader(SaleFileList.get(i));
				br = new BufferedReader(fr);
				String saleLine;
				while((saleLine = br.readLine()) != null) {
					Sales.add(saleLine);
				}
				//売上ファイルの行数が2行でない場合
				if(Sales.size() != 2){
					String fileName = SaleFileList.get(i).getName();
					System.out.println(fileName + "のフォーマットが不正です");
					return;
				//mapにキーが存在するかの確認
				}else if(nameMap.containsKey(Sales.get(0))){
					//"^[0-9]+$" ← 数字だけって意味
					if(Sales.get(1).matches("^[0-9]+$")) {
						// s=売上金額
						long s = Long.parseLong(Sales.get(1));
						total = s + saleMap.get(Sales.get(0));
						saleMap.put(Sales.get(0), total);
						//10桁超えたら
						if(total > 9999999999L) {
							System.out.println("合計金額が10桁を超えました");
							return;
						}
					}else {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}else {
					String fileName = SaleFileList.get(i).getName();
					System.out.println(fileName+"の支店コードが不正です");
					return;
				}
			}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
		}finally{
			if(br != null) {
				try {
					br.close();
					fr.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}

		if(!output(args[0],"branch.out",nameMap,saleMap)) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
	}

	public static boolean output(String directry,String fileName, HashMap<String,String> nameMap,HashMap<String,Long> saleMap){
		BufferedWriter bw = null;
		try {
			File out = new File(directry,fileName);
			FileWriter fw = new FileWriter(out);
			bw = new BufferedWriter(fw);
			for(String key : nameMap.keySet()){
				bw.write(key + "," + nameMap.get(key) + "," + saleMap.get(key));
				bw.newLine();
			}
		}catch(IOException e) {
			return false;
		}finally {
			if(bw != null) {
				try {
					bw.close();
				}
				catch(IOException e){
					return false;
				}
			}
		}
		return true;
	}
}